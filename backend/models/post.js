const mongoose = require('mongoose');
const postSchema = mongoose.Schema({
    name: { type: String, required: true},
    email: { type: String, required: true },
    mobile: { type: String, required: true },
    address: { type: String, required: true }
});

module.exports = mongoose.model('Post',postSchema); //Post is predefined in mongoose so we need to write it like that