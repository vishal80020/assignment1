const express = require('express');
const bodyParser = require("body-parser");


const mongoose = require("mongoose");


//importing our mongoose schema
const Post = require('./models/post');

const app = express();

//connecting your mongoDb
mongoose.connect("mongodb+srv://Vishal:yVU943qU87mlnCEX@cluster0.qasgg.mongodb.net/node-angular?retryWrites=true&w=majority",{useNewUrlParser:true,useUnifiedTopology:true})
.then(()=> {
    console.log('connected to database!');
})
.catch(()=>{
    console.log('connection failed!')
});

app.use(bodyParser.json());

//to handle the CORS error
app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin","*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin,X-Requested-With,Content-Type,Accept");
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET,POST,PATCH,PUT,DELETE,OPTIONS"
    );
    next();
});


app.post('/api/posts',(req,res,next)=>{
    const post = new Post({
        name: req.body.name,
        email: req.body.email,
        mobile: req.body.mobile,
        address: req.body.address
    });
    //const post =req.body
    // //console.log(post);

   
    post.save().then(createdPost =>{
        res.status(201).json({
            message: "Post added successfully",
            postId: createdPost._id // here I am passing post returned by monogodb
        });

    });
    
});


app.put("/api/posts/:id",(req,res,next) => {
    const post = new Post({
        _id: req.body.id,
        name:req.body.name,
        email:req.body.email,
        mobile:req.body.mobile,
        address:req.body.address
    });
    Post.updateOne({_id: req.params.id},post).then(result =>{
        console.log(result);
        res.status(200).json({ message: "Updated successfully"});
    });
});





app.get('/api/posts',(req,res,next)=>{
    Post.find().then(documents => {
        res.status(200).json({
            message: "Posts fetched successfully",
            posts:documents
        });
    });

});


//After getting pre-populated page for editing,
//on refreshing the page we will lose the data entered
//in the form but we have id in the URL
//To avoid that

app.get("/api/posts/:id",(req,res,next) =>{
    //Post model in moongoose schema has method findViewBy id
    // not _id in req.params because it is from URL
    Post.findById(req.params.id).then(post=>{
        if(post) {
            res.status(200).json(post);
        }else {
            req.status(404).json({message: "Post not found!"});
        }
    });
});




app.delete("/api/posts/:id",(req, res, next) => {
    //to delete from Database
    Post.deleteOne({_id:req.params.id}).then(result=>{
        console.log(result);
        res.status(200).json({message:"Post Deleted!"});
    });
    
});


module.exports =app;