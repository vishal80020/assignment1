import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Post } from "../post.model";

import { PostsService } from "../posts.service";

@Component({
  selector: "app-post-create",
  templateUrl: "./post-create.component.html",
  styleUrls: ["./post-create.component.css"]
})
export class PostCreateComponent implements OnInit{
  post:Post;
  private mode='create';
  private postId:string;

  constructor(public postsService: PostsService,
    public route:ActivatedRoute) {}




ngOnInit(){
  this.route.paramMap.subscribe((paramMap:ParamMap) =>{
    if(paramMap.has('postId')){
      this.mode='edit';
      this.postId=paramMap.get('postId');
      this.postsService.getPost(this.postId).subscribe(postData =>{
        this.post = { id: postData._id,
        name: postData.name,
        email: postData.email,
        mobile: postData.mobile,
        address: postData.address        
      };

      });
      
    }else{
      this.mode='create';
      this.postId=null;
    }
  });
}

  onAddPost(form: NgForm) {
    if (form.invalid) {
      return;
    }
    if(this.mode==='create'){
      console.log(form.value.name, form.value.email, form.value.mobile, form.value.address)
      this.postsService.addPost(form.value.name, form.value.email, form.value.mobile, form.value.address );
    } else {
      this.postsService.upDatePost(
        this.postId,
        form.value.name,
        form.value.email,
        form.value.mobile,
        form.value.address
      );
    }


    
    form.resetForm();
  }
}
