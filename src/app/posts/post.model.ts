export interface Post {
    id:string;
    name:string;
    email:string;
    mobile:string;
    address:string;
  }
  