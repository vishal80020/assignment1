import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators'

import { Post } from './post.model';

@Injectable({providedIn: 'root'})
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();

  constructor(private http:HttpClient,
    private router :Router){}

  getPosts() {
    this.http.get<{ message: string, posts: any}>("http://localhost:3000/api/posts"
    )
    .pipe(map((postData)=>{
      return postData.posts.map(post =>{
        return {
          name:post.name,
          email:post.email,
          mobile:post.mobile,
          address:post.address,
          id:post._id
        };
      });
  }))
  .subscribe((transformedPosts) => {
    this.posts=transformedPosts;
    this.postsUpdated.next([...this.posts]);
  });

  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }


  
//for getting edited post
getPost(id:string){
  //return {...this.posts.find(p => p.id===id)};
  return this.http.get<
  {
    _id:string,
    name:string,
    email:string,
    mobile:string,
    address:string
  }>("http://localhost:3000/api/posts/"+id);
}


  addPost(name: string, email: string, mobile: string,address:string) {
    const post: Post = { id: null, name: name, email: email, mobile: mobile, address:address };
    this.http.post<{message:string,postId:string}>("http://localhost:3000/api/posts",post)
    .subscribe((responseData)=>{
      console.log(responseData.message)
      const id=responseData.postId;
      post.id =id;
    this.posts.push(post);
    this.postsUpdated.next([...this.posts]);
    this.router.navigate(["/"]);
    });
  }


  deletePost(postId:string){
    this.http.delete("http://localhost:3000/api/posts/"+postId)
    .subscribe(()=>{
      //to update the UI
      const updatedPosts=this.posts.filter(post => post.id!==postId);
      this.posts =updatedPosts;
      this.postsUpdated.next([...this.posts]);
      console.log('Deleted');
    });
  }

  upDatePost(id:string,name: string, email: string, mobile: string, address:string){
    const post: Post ={ id:id, name:name,  email: email, mobile: mobile, address:address };
    this.http
    .put("http://localhost:3000/api/posts/"+id,post)
    .subscribe(response => {console.log(response)
      this.router.navigate(["/"]);
    });
  }


}
